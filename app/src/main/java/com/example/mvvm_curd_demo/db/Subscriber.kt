package com.anushka.roomdemo_MVVM.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "subscriber_data_table")
data class Subscriber (

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "name")
    var id : Int,

    @ColumnInfo(name = "id")
    var name : String,

    @ColumnInfo(name = "email")
    var email : String

)