package com.example.mvvm_curd_demo

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.anushka.roomdemo_MVVM.db.Subscriber
import com.example.mvvm_curd_demo.databinding.CustomListItemsBinding
import kotlinx.android.synthetic.main.custom_list_items.view.*

class MyAdapter(private val clickListener:(Subscriber)->Unit)
    :RecyclerView.Adapter<MyAdapter.ViewHolder>() {

    private val subscriberList = ArrayList<Subscriber>()


    class ViewHolder(val binding: CustomListItemsBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(subscriber: Subscriber,clickListener: (Subscriber) -> Unit){
            binding.root.name_text_view.text = subscriber.name
            binding.root.email_text_view.text = subscriber.email
            binding.root.list_item_layout.setOnClickListener {
                clickListener(subscriber)
            }
        }

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val binding : CustomListItemsBinding =
            DataBindingUtil.inflate(layoutInflater,R.layout.custom_list_items,parent,false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {

        return subscriberList.size

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(subscriberList[position],clickListener)

    }
    fun setList (subscribers:List<Subscriber>){
        subscriberList.clear()
        subscriberList.addAll(subscribers)
    }
}