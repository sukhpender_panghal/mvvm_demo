package com.anushka.roomdemo_MVVM

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.anushka.roomdemo_MVVM.db.Subscriber
import com.anushka.roomdemo_MVVM.db.SubscriberDatabase
import com.anushka.roomdemo_MVVM.db.SubscriberRepository
import com.example.mvvm_curd_demo.R
import com.example.mvvm_curd_demo.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var subscriberViewModel: SubscriberViewModel
    private lateinit var adapter: MyRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this,
            R.layout.activity_main
        )

        val dao = SubscriberDatabase.getInstance(application).subscriberDAO
        val repository = SubscriberRepository(dao)
        val factory =
            SubscriberViewModelFactory(repository)

        subscriberViewModel = ViewModelProvider(this,factory).get(SubscriberViewModel::class.java)
        binding.myViewModel = subscriberViewModel
        binding.lifecycleOwner = this
        initRecyclerView()

        subscriberViewModel.message.observe(this, Observer {
         it.getContentIfNotHandled()?.let {
             Toast.makeText(this, it, Toast.LENGTH_LONG).show()
         }
        })

    }

   private fun initRecyclerView(){

       binding.subscriberRecyclerView.layoutManager = LinearLayoutManager(this)
       adapter =
           MyRecyclerViewAdapter({ selectedItem: Subscriber ->
               listItemClicked(selectedItem)
           })
       binding.subscriberRecyclerView.adapter = adapter
       displayList()
   }

    private fun displayList(){

        subscriberViewModel.subscribers.observe(this, Observer {
            Log.i("data",it.toString())
            adapter.setList(it)
            adapter.notifyDataSetChanged()
        })
    }

    private fun listItemClicked(subscriber: Subscriber){
        subscriberViewModel.initUpdateAndDelete(subscriber)
    }
}
